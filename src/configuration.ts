import * as dotenv from 'dotenv';

dotenv.config();

export interface Config {
  veramoPort: number;
  libp2pApi: string;
}

export const loadConfig = (): Config => {
  if (process.env.VERAMO_PORT) {
    return {
      veramoPort: parseInt(process.env.VERAMO_PORT, 10),
      libp2pApi: process.env.LIBP2P_API || "localhost:10001",
    };
  }
  return {
    veramoPort: 10000,
    libp2pApi: process.env.LIBP2P_API || "localhost:10001",
  };
};
