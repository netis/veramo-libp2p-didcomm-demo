package main

import "time"

const (

	// P2PProtoPrefix is the default required prefix for protocol names
	P2PProtoPrefix = "/x/"

	cmgrTag = "stream-fwd"

	resolveTimeout = 10 * time.Second
)

var configNetworkDemo = ConfigNetwork{
	Name:   "aceblock-libp2p-demo",
	Type:   "permissioned",
	Secret: "/key/swarm/psk/1.0.0/\n/base16/\n2c6c69741f95ed59f5a661c76d21d5befb6f95422b9ea6d860d257857755b824",
	Bootstrap: []string{
		"/ip4/91.240.216.80/tcp/9001/p2p/12D3KooWAqMbnbWPZ6NGVFuxLvvfMoVF2MbAtosLL9nkCYvUraHt",
		"/ip4/91.240.216.81/tcp/9001/p2p/12D3KooWFByj7GYyQtSYs7aGovzCkCsxJ56UyfASPiUGvusqbPy8",
		"/ip4/91.240.216.114/tcp/443/p2p/12D3KooWSSBaGtGuLiciQzeq4cQpPqtbp96XSDbfCxpGx7tenrpA",
		"/ip4/91.240.216.200/tcp/443/p2p/12D3KooWCwWCt42Z5SLpT9QzESLXx962TuyqvX7SrqE8Tway4qCm",
		"/ip4/91.240.216.201/tcp/9001/p2p/12D3KooWED7tZH3H2JFHHF7kGq2pyNtq7JG9TunjjysCMXCULepV",
	},
}
