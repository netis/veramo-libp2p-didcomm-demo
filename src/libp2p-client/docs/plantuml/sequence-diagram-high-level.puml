@startuml sequence-diagram-high-level

title DIDComm via LibP2P - A high-level sequence diagram

actor "Alice" as A
box Alice's environment
participant "Alice's Agent" as AA
participant "Alice's LibP2P Client" as AL
end box

participant "Relay Nodes" as RN

box Bob's environment
participant "Bob's LibP2P Client" as BL
participant "Bob's Agent" as BA
end box
actor "Bob" as B

participant "Universal Resolver" as UR

== Exchange DIDs via an authenticated channel ==

A -> B: Exchange with Bob: did:x:alice
A <- B: Exchange with Alice: did:x:bob

== Obtain DID Documents ==

alt Resolve DIDs

	A -> UR: GET /universal-resolver/v1/identifiers/did:x:bob
	return DID Document

	B -> UR: GET /universal-resolver/v1/identifiers/did:x:alice
	return DID Document

else exchange DID Documents

	A -> B: Send DID Document
	A <- B: Send DID Document

end

== Establish P2P stream ==


alt Direct Stream
	AL -> BL: connect: /p2p/<bob-peer-id> protocol: /x/<protocol-name>
	activate BL
	AL <-> BL: mTLS (mutual TLS authentication using DID keys); TLS V1.3
	AL -> AL: Verify that auth. key belongs to Bob's DID
	BL -> BL: Verify that auth. key belongs to Alice's DID
	AL <-> BL: End-to-end encrypted stream with perfect forward secrecy (TLS v1.3 or Noise Protocol)
	deactivate BL

else Relayed Stream (both clients are behind NAT/Firewall)
	AL -> RN: connect to a relay node /p2p/<relay-node-id>
	AL <-> RN:  mTLS (mutual TLS authentication using DID keys); TLS V1.3
	BL -> RN: connect to a relay node /p2p/<relay-node-id>
	BL <-> RN:  mTLS (mutual TLS authentication using DID keys); TLS V1.3

	AL -> BL: Connect via relay node /p2p-circuit/ipfs/<bob-peer-id> protocol: /x/<protocol-name>
	AL <-> BL: mTLS (mutual TLS authentication using DID keys); TLS V1.3
	AL -> AL: Verify that auth. key belongs to Bob's DID
	BL -> BL: Verify that auth. key belongs to Alice's DID
	AL <-> BL: End-to-end encrypted stream with perfect forward secrecy (TLS v1.3 or Noise Protocol)
end

== DIDComm example - forward communication and start exchanging messages ==

AL -> AL: Listen to port 4000 and forward to <p2p>/<bob-peer-id>
BL -> BA: Listen to port p2p/<bob-peer-id>\nand forward to localhost:5000
BA -> BA: DIDComm Agent listens to localhost:5000

AA -> AA: Connect to Bob's Agent via localhost:4000
AA -> AL: DIDComm Agent traffic from localhost:4000 -> p2p/<bob-peer-id>
AL -> BL: DIDComm Agent traffic (stream is protocol independent)
BL -> BA: DIDComm Agent traffic from p2p/<bob-peer-id> to localhost:5000

== Decentralized Signal-Protocol messaging ==

AA -> AL: Share Signal key bundles with Bob
AL -> BL: forward the Signal key bundles
BL -> BA: Signal key bundles

BA -> BL: Share Signal key bundles with Alice
BL -> AL: forward the Signal key bundles
AL -> AA: Signal key bundles

note over AA
	Alice and Bob can exchange in sycn or async mode
	messages protected with X3DH and Double Ratchet.
end note

@enduml