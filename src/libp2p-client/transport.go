package main

import (
	"net/http"

	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
)

// MakeHandler
// Create all the REST HTTP endpoints
func MakeHandler(node *Node) http.Handler {

	// POST /connections
	newConnection := kithttp.NewServer(
		ConnectToEndpoint(node),
		decodeConnectToRequest,
		encodeResponse,
	)

	// GET /listeners
	getListeners := kithttp.NewServer(
		ListActiveListenersEndpoint(node),
		decodeListActiveListenersRequest,
		encodeResponse,
	)

	// POST /listeners
	newListener := kithttp.NewServer(
		ListenToEndpoint(node),
		decodeListenToRequest,
		encodeResponse,
	)

	// GET /peers
	getPeers := kithttp.NewServer(
		GetPeersEndpoint(node),
		decodeGetPeersRequest,
		encodeResponse,
	)

	// GET /properties/did
	getDID := kithttp.NewServer(
		NodeDIDEndpoint(node),
		decodeNodeDIDRequest,
		encodeResponse,
	)

	// GET /properties/did-document
	getDIDDocument := kithttp.NewServer(
		NodeDIDDocumentEndpoint(node),
		decodeNodeDIDDocumentRequest,
		encodeResponse,
	)
	// GET /properties/did-services
	getDIDDocumentService := kithttp.NewServer(
		NodeDIDDocumentServiceEndpoint(node),
		decodeNodeDIDDocumentServiceRequest,
		encodeResponse,
	)

	router := mux.NewRouter()
	router.Handle("/libp2p/v1/connections", newConnection).Methods("POST")

	router.Handle("/libp2p/v1/listeners", getListeners).Methods("GET")
	router.Handle("/libp2p/v1/listeners", newListener).Methods("POST")

	router.Handle("/libp2p/v1/peers", getPeers).Methods("GET")

	router.Handle("/libp2p/v1/properties/did", getDID).Methods("GET")
	router.Handle("/libp2p/v1/properties/did-document", getDIDDocument).Methods("GET")
	router.Handle("/libp2p/v1/properties/did-service", getDIDDocumentService).Methods("GET")

	return router
}
