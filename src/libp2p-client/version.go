package main

import "fmt"

// Package Info

var (
	lastCommit     string
	buildTimestamp string //
)

// Constants
const (
	_name      = "Libp2p client"        // Name
	_status    = "dev"                  // Status: dev, stable, release, err/broken/bug
	VERSION    = "0.1.0"                // Current version
	_developer = "alen.horvat@netis.si" // Who is working on the component
	LICENSE    = "MIT"                  // License
)

// Banner prints the software info
func Banner() {
	fmt.Println("********************************************************************************")
	fmt.Printf("  Name:\t\t%s\n", _name)
	fmt.Printf("  Status:\t%s\n", _status)
	fmt.Printf("  Version:\tv%s\n", VERSION)
	fmt.Printf("  Dev:\t\t%s\n", _developer)
	fmt.Printf("  License:\t%s\n", LICENSE)
	fmt.Printf("  Last Commit:\t%s\n", lastCommit)
	fmt.Printf("  Build Timestamp: %s\n", buildTimestamp)
	fmt.Println("********************************************************************************")
}
