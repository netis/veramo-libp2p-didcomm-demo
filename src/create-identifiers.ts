import { agent, AliasPortMap, UserInfo } from "./veramo/setup";
import { MessagingRouter, RequestWithAgentRouter } from "@veramo/remote-server";
import express, { Request, Response } from "express";
import { computeAddress } from "@ethersproject/transactions";

import bodyParser from "body-parser";
import http from "http";
import path from "path";
import { DIDDocument, IIdentifier } from "@veramo/core";

import { loadConfig } from "./configuration";
const { veramoPort, libp2pApi } = loadConfig();

async function main() {
  console.log(libp2pApi);
  console.log(veramoPort);
  // Start DIDComm router
  let didCommEndpointServer: http.Server;
  const requestWithAgent = RequestWithAgentRouter({ agent });
  // Create a random local port
  const didCommPort = await Math.round(Math.random() * 32000 + 2048);
  await new Promise(async (resolve) => {
    //setup a server to receive HTTP messages and forward them to this agent to be processed as DIDComm messages
    const app = express();
    // app.use(requestWithAgent)

    // Start the local listener (@Alen)
    // The service listens on the libp2p network and forwards the connections to a local service (localhost:didCommPort)
    // TODO: it may take some time to star the libp2p; we need to ensure the libp2p is up before we call the endpoint
    try {
      const response = await createLocalListener(didCommPort);
      console.log(
        `[libp2p] local listener registered: localhost:${didCommPort}`
      );
    } catch (error) {
      console.log(error);
      return;
    }

    // Create a local endpoint
    app.use(
      "/messaging",
      requestWithAgent,
      MessagingRouter({
        metaData: { type: "DIDComm", value: "integration test" },
      })
    );
    didCommEndpointServer = app.listen(didCommPort, () => {
      console.log(
        `DIDComm endpoint running at http://localhost:${didCommPort}/messaging`
      );
      resolve(true);
    });
    // TODO: @Mircea - how do we handle the incomming messages?
  });

  // Start API server
  const app = express();
  var jsonParser = bodyParser.json();

  // Show static ui
  app.get("/", function (req, res) {
    res.sendFile(path.join(__dirname, "static-ui/index.html"));
  });
  // Create a new user
  app.post("/veramo/messaging/v1/users", jsonParser, postUsersV1);
  // Return user info (DID Document)
  app.get("/veramo/messaging/v1/users/:alias", jsonParser, getUserByAlias);
  // Create a new service
  app.post(
    "/veramo/messaging/v1/users/:alias/connections",
    jsonParser,
    postUsersV1Connections
  );
  // Send message
  app.post(
    "/veramo/messaging/v1/users/:alias/receivers/:receiver_alias/messages",
    jsonParser,
    postUsersV1Messages
  );

  http
    .createServer(app)
    .listen(veramoPort, () =>
      console.log(`Veramo API running at http://localhost:${veramoPort}/`)
    );
}

export interface UserAlias {
  alias: string;
}

export interface libp2pService {
  id: string;
  type: string;
  routingKeys: string[];
  serviceEndpoint: string;
  accept: string[];
}

// postUsersV1 create a new user
// Endpoint: /users/v1
const postUsersV1 = async (req: Request, response: Response) => {
  try {
    // Obtain user alias
    const alias: UserAlias = req.body;
    let identity: IIdentifier;

    // Check if user exists
    try {
      identity = await agent.didManagerGetByAlias({ alias: alias.alias });
    } catch (error) {
      // User doesn't exist, create a new user
      identity = await agent.didManagerCreate({
        alias: alias.alias,
        kms: "local",
      });
    }

    // Get libp2p service info to update the service endpoint
    let services: libp2pService[];
    try {
      services = await getLibp2pServices();
    } catch (error) {
      console.log(error);
      response.status(500);
      response.send("Internal error - libp2p service unreachable.");
      return;
    }

    // TODO: At the moment we cannot publish the relay node info in an efficient way
    // For the purpose of the demo we assume clients connect to the same relay nodes
    for (let service of services) {
      try {
        await agent.didManagerAddService({
          did: identity.did,
          service: {
            id: "did:ethr:rinkeby:libp2p",
            type: "DIDCommMessaging",
            serviceEndpoint: service.serviceEndpoint,
            description: service.routingKeys.toString(),
          },
        });
      } catch (error) {
        console.log(`ERROR: ${error}`);
        response.status(400);
        const address = await computeAddress(
          "0x" + identity.keys[0].publicKeyHex
        );
        response.send(
          `Failed to update the DID service. Check if address ${address} has enough funds. Provider: ${identity.provider}`
        );
        return;
      }
    }
    // Return the identity
    response.status(200).json(identity);
  } catch (error) {
    console.log(error);
    response.status(400);
    response.send("Invalid request.");
    return;
  }
};

async function getLibp2pServices(): Promise<libp2pService[]> {
  // For now, consider the data is stored on a static `users.json` file
  let uri = `http://${libp2pApi}/libp2p/v1/properties/did-service`;
  console.log(uri);
  return (
    fetch(uri)
      // the JSON body is taken from the response
      .then((res) => res.json())
      .then((res) => {
        return res as libp2pService[];
      })
  );
}

async function createLocalListener(targetPort: number): Promise<any> {
  // For now, consider the data is stored on a static `users.json` file
  let uri = `http://${libp2pApi}/libp2p/v1/listeners`;
  const data = {
    Protocol: "/x/didcomm/v2",
    TargetAddress: `/ip4/127.0.0.1/tcp/${targetPort}`,
  };

  console.log(uri);
  return (
    fetch(uri, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    })
      // the JSON body is taken from the response
      .then((res) => res.json())
      .then((res) => {
        return res;
      })
  );
}

// Get DID Document by alias
// endpoint: /users/v1/{alias}
const getUserByAlias = async (req: Request, response: Response) => {
  try {
    // Obtain user alias
    const _alias: string = req.params.alias;
    console.log(_alias);
    const identity = await agent.didManagerGetByAlias({ alias: _alias });
    // Return the identity
    const didDoc = await agent.resolveDid({ didUrl: identity.did });
    response.status(200).json(didDoc);
  } catch (error) {
    response.status(400);
    response.send("Invalid request.");
  }
};

export interface did {
  did: string;
  alias: string;
}

export interface libp2pConnectionRequest {
  Protocol: string; // Protocol name (in terms of libp2p); e.g., /x/didcomm/v2
  ListenAddress: string; // Local address to which we send the messages
  TargetAddress: string; // Remote libp2p address to which we forward the messages
}

// Connect with a peer via libp2p
// Endpoint: /users/v1/:alias/connections
// Header: "Content-Type: application/json"
// Body:
//   did: user did
//   alias: string; -- receiver
const postUsersV1Connections = async (req: Request, response: Response) => {
  // Resolve the DID we're trying to connect with
  try {
    const _did: did = req.body;
    const reciverAlias = _did.alias;
    const _alias: string = req.params.alias;
    const didDocRes = await agent.resolveDid({ didUrl: _did.did });
    let didDoc: DIDDocument;
    if (didDocRes.didDocument) {
      didDoc = didDocRes.didDocument;
    } else {
      response.status(400);
      response.send("Invalid request.");
      return;
    }

    // Extract the libp2p service endpoint
    let serviceEndpoint: string;
    if (didDoc.service?.[0].serviceEndpoint) {
      serviceEndpoint = didDoc.service?.[0].serviceEndpoint;
    } else {
      response.status(400);
      response.send("Invalid request.");
      return;
    }

    // Create a random local port
    const listeningPort = Math.round(Math.random() * 32000 + 2048);
    // TODO: store the alias : port mapping locally

    const lcr: libp2pConnectionRequest = {
      Protocol: "/x/didcomm/v2", // libp2p protocol definition
      ListenAddress: `/ip4/127.0.0.1/tcp/${listeningPort}`, // we're sending messages to this endpoint
      TargetAddress: serviceEndpoint, // Remote libp2p address to which we're sending messages
    };

    // Establish a libp2p connection
    let uri = `http://${libp2pApi}/libp2p/v1/connections`;
    console.log(uri);
    fetch(uri, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(lcr),
    });
    // TODO: check the response

    // Store the alias port mapping
    const ui: UserInfo = {
      did: _did.did,
      port: listeningPort,
    };
    AliasPortMap.set(reciverAlias, ui);

    response.status(200);
    response.send("Connected.");
  } catch (error) {
    response.status(400);
    response.send("Invalid request.");
    return;
  }
};

export interface NewMessage {
  type: string;
  id: string;
  body: object;
}

// Send message
const postUsersV1Messages = async (request: Request, response: Response) => {
  try {
    const sender: string = request.params.alias;
    const receiver: string = request.params.receiver_alias;
    // TODO: return error if not found
    const ui: UserInfo = AliasPortMap.get(receiver);

    const message: NewMessage = request.body;
    const identity = await agent.didManagerGetByAlias({ alias: sender });

    // Pack a message
    const packedMessage = await agent.packDIDCommMessage({
      packing: "jws",
      message: {
        from: identity.did, // get our did
        to: ui.did,
        type: message.type,
        id: message.id,
        body: message.body,
      },
    });

    // Send a message
    let uri = `http://localhost:${ui.port}/messaging`;
    console.log(uri);
    fetch(uri, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(packedMessage),
    });
    // TODO: handle the response
    response.status(200);
  } catch (error) {
    response.status(400);
    response.send("Invalid request.");
  }
};

main().catch(console.log);
