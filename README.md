# Veramo DIDComm

## Build

Clone the repository

Run: yarn

Install GO

Run: yarn libp2p:init

Run: yarn libp2p:build

Run: yarn didcomm:start

Modify the .env (ensure the ports are available)

## Demo

Alice:

curl -X POST localhost:8888/veramo/messaging/v1/users -d '{"alias": "alice"}' -H "Content-Type: application/json"

top up the account (rinkeby; e.g., 0.1 ETH)

```bash
curl -X POST localhost:8888/veramo/messaging/v1/users -d '{"alias": "alice"}' -H "Content-Type: application/json"
```

Bob:
Same steps as for alice

Exchange DIDs out of band
Alice's DID: did:ethr:rinkeby:0x02a775752d11ceb225b5b1e8ccc8492fb3a50e25d3d144506a3f4fec6bc6b41d37
Bob's DID: did:ethr:rinkeby:0x03d1023345322b87c0f840eb80af4e57dde30b3d4a43d5003594200db0c3033a75

Alice:

Connect with Bob

```bash
curl -X POST localhost:8888/veramo/messaging/v1/users/alice/connections -d '{"alias":"bob", "did":"did:ethr:rinkeby:0x03d1023345322b87c0f840eb80af4e57dde30b3d4a43d5003594200db0c3033a75"}' -H "Content-Type: application/json"
```

Send message

```bash
curl -X POST localhost:8888/veramo/messaging/v1/users/alice/receivers/bob/messages -H "Content-Type: application/json" -d '{"type":"test", "id":"test-jws-success", "body":"HelloWorld!"}'
```

## Endpoints and flow

POST /users/v1
body: alias
Response: DID Document

- create DID
- create peerID + service info (libp2p)

## Flow

1. Create a new local user (POST /users/v1)
2. Obtain the DID/DIDDocument (GET /users/v1/{alias}) (alias is the local user alias)
   body: {"did": did}
3. Exchange DID/DID Doc. out-of-band
4. Connect
5. Send messages

POST /users/v1
